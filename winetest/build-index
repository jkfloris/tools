#!/usr/bin/perl
#
# Copyright 2008 Alexandre Julliard <julliard@winehq.org>
# Copyright 2014-2021 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use strict;
use warnings;
use open ':utf8';
use CGI qw(:standard);

sub BEGIN
{
    if ($0 !~ m=^/=)
    {
        # Turn $0 into an absolute path so it can safely be used in @INC
        require Cwd;
        $0 = Cwd::cwd() . "/$0";
    }
    unshift @INC, $1 if ($0 =~ m=^(/.*)/[^/]+$=);
}
use vars qw/$workdir $gitdir $gitlab @groups/;
require "winetest.conf";

my $name0=$0;
$name0 =~ s+^.*/++;


#
# Common helpers
#

sub error(@)
{
    print STDERR "$name0:error: ", @_;
}

$ENV{GIT_DIR} = $gitdir;

sub get_build_info($)
{
    my ($build) = @_;
    my ($date, $subject);

    my $commit = `git log --max-count=1 --pretty="format:%ct %s" "$build^0" 2>/dev/null` if ($build =~ /^[0-9a-f]{40}$/);
    if ($commit && $commit =~ /^(\d+) (.*)$/)
    {
        ($date, $subject) = ($1, $2);
        # Make sure the directory's mtime matches the commit time
        utime $date, $date, "data/$build";
    }
    else
    {
        $date = (stat "data/$build")[9];
        $subject = "";
    }
    return ($date, $subject);
}

use POSIX qw(locale_h strftime);
setlocale(LC_ALL, "C");

sub short_date($)
{
    my ($date) = @_;
    return strftime("%b&nbsp;%d", gmtime($date));
}


#
# Command line processing
#

my ($opt_workdir, $usage);

sub check_opt_val($$)
{
    my ($option, $val) = @_;

    if (defined $val)
    {
        error("$option can only be specified once\n");
        $usage = 2; # but continue processing this option
    }
    if (!@ARGV)
    {
        error("missing value for $option\n");
        $usage = 2;
        return undef;
    }
    return shift @ARGV;
}

while (@ARGV)
{
    my $arg = shift @ARGV;
    if ($arg eq "--workdir")
    {
        $workdir = $opt_workdir = check_opt_val($arg, $opt_workdir);
    }
    elsif ($arg eq "--help")
    {
        $usage = 0;
    }
    else
    {
        error("unknown argument '$arg'\n");
        $usage = 2;
    }
}
if (!defined $usage)
{
    if (!defined $workdir)
    {
        require Cwd;
        $workdir = Cwd::cwd();
    }
    elsif ($workdir !~ m%^/%)
    {
        require Cwd;
        $workdir = Cwd::cwd() . "/$workdir";
    }
    if (!-f "$workdir/report.css")
    {
        error("'$workdir' is not a valid work directory\n");
        $usage = 2;
    }
}
if (defined $usage)
{
    if ($usage)
    {
        error("try '$name0 --help' for more information\n");
        exit $usage;
    }
    print <<EOF;
Usage: $name0 [--workdir DIR] [--help]

Processes the build summaries to generate the global index and per-test
index pages.

Where:
  --workdir DIR     Specifies the directory containing the winetest website
                    files. Can be omitted if set in winetest.conf.
  --help            Shows this usage message.

Actions:
  $name0 should be called after the gather script has updated all the builds.

Generated files:
  \$workdir/data/index.html
  \$workdir/data/tests/TEST:UNIT.html

Exit:
  0 - success
  2 - usage error
EOF
    exit 0;
}

chdir($workdir) or die "could not chdir to the work directory: $!";


#
# Grab the build list and archive the old results
#

my @builds;
my @too_old;

opendir(DIR, "data") or die "could not open the 'data' directory: $!";
foreach my $build (readdir(DIR))
{
    if ($build !~ /^[0-9a-f]{40}$/)
    {
        if ($build !~ /^(?:\.\.?|(?:errors|index|patterns.*)\.html|tests)$/)
        {
            error("'data/$build' is not a valid build directory\n");
        }
        next;
    }
    next unless -f "data/$build/index.html";

    my ($date, $subject) = get_build_info($build);
    if (time() - $date > 60 * 24 * 60 * 60)
    {
        # Archive builds older than 60 days
        push @too_old, $build;
    }
    else
    {
        push @builds, { name => $build, date => $date, subj => $subject };
    }
}

closedir(DIR);
@builds = sort { $b->{date} <=> $a->{date} } @builds;

# remove the too old results
foreach my $build (@too_old)
{
    if (!rename "data/$build", "old-data/$build")
    {
        error("could not move 'data/$build' to old-data: $!\n");
    }
}


#
# Read each build's summary.txt file
# This gets us the statistics for the global per-test-unit index.
#

my %alltests = ();
my %haspattern = ();

foreach my $build (@builds)
{
    open SUM, "data/$build->{name}/summary.txt" or next;
    while (<SUM>)
    {
        chomp;
        my ($test, $group, $cell) = split / +/, $_, 3;
        $alltests{$test}->{$build->{name}}->{$group} = $cell;
        if (!$haspattern{$test} and $cell =~ /result (?:fail|mixed)/)
        {
            $haspattern{$test} = 1;
        }
    }
    close SUM;
}


#
# Link the build the test units together
#

my $prev;
foreach my $test (sort keys %alltests)
{
    $alltests{$test}->{prev} = "$prev.html" if $prev;
    $alltests{$prev}->{next} = "$test.html" if $prev;
    $prev = $test;
}


#
# Write the test unit index pages
#

foreach my $test (keys %alltests)
{
    my $filename = "data/tests/$test.html";
    open OUT, ">", "$filename.new" or die "could not open '$filename.new' for writing: $!";

    my $title_link = $haspattern{$test} ? " | <a href=\"/data/patterns.html#$test\">failure patterns</a>" : "";
    print OUT <<EOF;
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
                      "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>$test test runs</title>
  <link rel="stylesheet" href="/summary.css" type="text/css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div class="navbar">
EOF
    print OUT defined($alltests{$test}->{prev}) ? "<a href=\"./$alltests{$test}->{prev}\">prev</a>" : "prev";
    print OUT defined($alltests{$test}->{next}) ? " | <a href=\"./$alltests{$test}->{next}\">next</a>" : " | next";
    print OUT <<EOF;
 | <a href="..">index</a>
</div>
<div class="main">
<h2>$test test runs$title_link</h2>
<table class="report">
<thead>
  <tr><th class="test">Build</th><th class="test">Date</th>
EOF

    # check which group names are actually used by that test
    my %used_group;
    foreach my $build (@builds)
    {
        next unless defined $alltests{$test}->{$build->{name}};
        foreach my $group (keys %{$alltests{$test}->{$build->{name}}}) { $used_group{$group} = 1; }
    }

    foreach my $group (@groups)
    {
        next unless defined $used_group{$group->{name}};
        printf OUT "  <th class=\"test\">$group->{name}</th>\n";
    }
    print OUT "  <th class=\"test\"></th></tr>\n";
    foreach my $build (@builds)
    {
        next unless defined $alltests{$test}->{$build->{name}};
        printf OUT "  <tr><td class=\"build\"><a href=\"../%s\" title=\"%s\">%s</a></td>\n",
                   $build->{name}, $build->{name}, substr($build->{name},0,12);
        printf OUT "  <td class=\"date\">%s</td>", short_date($build->{date});
        foreach my $group (@groups)
        {
            next unless defined $used_group{$group->{name}};
            if (defined $alltests{$test}->{$build->{name}}->{$group->{name}})
            {
                printf OUT "    %s\n", $alltests{$test}->{$build->{name}}->{$group->{name}};
            }
            else
            {
                print OUT "    <td class=\"note\"></td>\n";
            }
        }
        print OUT "  <td class=\"links\"><a href=\"$gitlab/commit/$build->{name}\">history</a>";
        print OUT "</td></tr>\n";
    }
    print OUT "</table></body></html>\n";
    close OUT;
    if (!rename "$filename.new", "$filename")
    {
        error("could not move '$filename.new' into place: $!\n");
        unlink "$filename.new";
    }
}

# And delete obsolete test files
opendir(DIR, "data/tests") or die "could not open the 'data/tests' directory: $!";
foreach my $entry (readdir(DIR))
{
    next if ($entry eq "." or $entry eq "..");
    my $test = $entry;
    $test =~ s/\.html$//;
    next if ($alltests{$test});
    if (!unlink "data/tests/$entry")
    {
        error("could not delete 'data/tests/$entry': $!\n");
    }

}
closedir(DIR);


#
# Read each build's total.txt file
# This gets us the statistics for the main index page.
#

my %versions = ();

foreach my $build (@builds)
{
    my %build_ver = ();
    if (open TOTAL, "data/$build->{name}/total.txt" )
    {
        while (<TOTAL>)
        {
            if (/^([A-Za-z0-9+]+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)(?:\s+(\d+))?/)
            {
                my ($name, $runs, $tests, $errors, $todos, $successes) = ($1, $2, $3, $4, $5, $6);
                $versions{$name}++;
                $build_ver{$name} = [ $runs, $tests, $errors, $todos, $successes ];
            }
        }
        close TOTAL;
    }
    $build->{versions} = \%build_ver;
}


#
# Write the global index page
#

my $filename = "data/index.html";
open OUT, ">", "$filename.new" or die "could not open '$filename.new' for writing: $!";

print OUT <<"EOF";
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
                      "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>Wine test runs</title>
  <link rel="stylesheet" href="/summary.css" type="text/css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div class="navbar">Failure patterns: <a href="patterns.html">all</a> | <a href="patterns-tb-win.html">testbot windows</a> | <a href="patterns-tb-wine.html">testbot wine</a></div>
<div class="main">
<h2>Wine test runs</h2>
EOF

print OUT "<table class=\"report\"><thead><tr><th class=\"test\">Build</th><th class=\"test\">Date</th>\n";
foreach my $ver (@groups)
{
    next unless defined($versions{$ver->{name}});
    printf OUT "<th class=\"test\">%s</th>", $ver->{name};
}
print OUT "<th colspan=\"3\">Failures</th><th></th></tr></thead>\n";

foreach my $build (@builds)
{
    printf OUT "  <tr><td class=\"build\"><a href=\"%s\" title=\"%s\">%s</a></td>\n", $build->{name}, $build->{name}, substr($build->{name},0,12);
    printf OUT "  <td class=\"date\">%s</td>", short_date($build->{date});
    my ($total_runs, $total_tests, $total_errors, $total_todos);
    foreach my $ver (@groups)
    {
        next unless defined($versions{$ver->{name}});
        my $counts = $build->{versions}->{$ver->{name}};
        if (!$counts || !@{$counts})
        {
            printf OUT "<td class=\"note\"> </td>";
        }
        else
        {
            my ($runs, $tests, $errors, $todos, $successes) = @{$counts};
            my $href = $runs > 1 ? "$build->{name}/index_$ver->{name}.html" : "$build->{name}";
            my $title = $runs > 1 ? "$runs test runs, " : "";
            $title .= "$tests unit tests, $errors have errors";
            $title .= ", $todos have todos" if ($todos);
            my $class = $errors ? ($successes ? "mixed" : "fail") . ($todos ? " also-todo" : "")
                                : ($todos ? "todo" : "pass");
            printf OUT "<td class=\"result %s\"><a title=\"%s\" href=\"%s\">%u</a></td>", $class, $title, $href, $errors || $todos;
            $total_runs++;
            $total_tests += $tests;
            $total_errors += $errors;
            $total_todos += $todos;
        }
    }
    if ($total_tests)
    {
        my $class = $total_errors ? "fail" : $total_todos ? "pass also-todo" : "pass";
        my $title = sprintf "%u test runs, %u total unit tests, %u have errors", $total_runs, $total_tests, $total_errors;
        $title .= ", $total_todos have todos" if $total_todos;
        printf OUT "\n  <td>&nbsp;</td><td class=\"result %s\"><a title=\"%s\" href=\"%s\">%4.1f%%</a></td><td>&nbsp;</td>\n",
                        $class, $title, $build->{name}, $total_errors * 100 / $total_tests;
    }
    else
    {
        print OUT "\n  <td>&nbsp;</td><td class=\"note\">&nbsp;</td><td>&nbsp;</td>\n";
    }
    print OUT "  <td class=\"commitlink\">";
    if ($build->{subj}) { printf OUT "<a href=\"$gitlab/commit/%s\">%s</a>", $build->{name}, escapeHTML($build->{subj}); }
    print OUT "</td></tr>\n";
}

print OUT <<"EOF";
</table></div>
<div class="navbar">
To run the tests on your Windows machine, download the <a href="https://gitlab.winehq.org/wine/wine/-/jobs/artifacts/master/raw/winetest.exe?job=build-winetest">latest winetest.exe</a> build.
If you have a 64-bit Windows OS, you can also run the <a href="https://gitlab.winehq.org/wine/wine/-/jobs/artifacts/master/raw/winetest64.exe?job=build-winetest">64-bit winetest</a>.<br>
If you don't see your results, check the <a href="errors.html">rejected reports</a> list.
</div>
</body></html>
EOF

close OUT;

if (!rename "$filename.new", "$filename")
{
    error("could not move '$filename.new' into place: $!\n");
    unlink "$filename.new";
}

exit 0;
