# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Provides a wrapper for the table JavaScript code.
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::CGI::Table;

use Exporter 'import';
our @EXPORT = qw(GetTableJSFile GenerateMasterCheckbox);


=pod
=over 12

=item C<GetTableJSFile()>

Returns the path to the file containing the table JavaScript code.

=back
=cut

sub GetTableJSFile()
{
  return "/js/table.js";
}


=pod
=over 12

=item C<GenerateMasterCheckbox()>

Generates a master checkbox that changes all the checkboxes with the specified
'cbgroup' value.

=over

=item CbGroup
A string identifying this group of checkboxes. The 'cbgroup' attribute of all
checkboxes in this group must be set to this value:

  <input type='checkbox' cbgroup='$CbGroup' name='...'>

=item Attrs
A string containing extra attributes to be inserted in this master checkbox
tag. It should not contain the type and hidden attributes.

=back
=back
=cut

sub GenerateMasterCheckbox($;$)
{
  my ($CbGroup, $Attrs) = @_;

  $Attrs ||= "";
  print "<input$Attrs type='checkbox' hidden='true' mcbgroup='$CbGroup'>\n";
}

1;
