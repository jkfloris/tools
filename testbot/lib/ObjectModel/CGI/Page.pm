# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Base class for web pages
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012, 2014, 2017-2018, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::CGI::Page;

=head1 NAME

ObjectModel::CGI::Page - Base class for web pages

This class provide a generic framework for generating web pages: retrieving
the HTTP parameters, manipulating cookies, checking the session, handling
errors, generating the HTTP header, etc.

It also defines their general structure: they shall be composed of a header, a
body and a footer. That structure can then be further refined by child classes
to suit their needs.

What it does not do is define what the page should look like: what the banner
should contain, which CSS files to reference, how to navigate from one page to
another, etc. It also does not define how to check that the user is allowed to
access the page or how to display error messages.

All these "look and feel" aspects are delegated to the PageBase object. Thus
many methods are just trampolines to their PageBase equivalent such that child
classes can call them and automatically get the appropriate behavior for the
actual website.

A typical use takes this form:

my $MyPage = MyPage->new($Request, "required-privileges");
$MyPage->GeneratePage();

=cut

use Exporter 'import';
our @EXPORT = qw(new SetPageBaseCreator);

use CGI;


my $PageBaseCreator;


=pod
=over 12

=item C<new()>

Creates a new page object.

Parameters:
=over
=item Request
This is the HTTP request leading to this page.

=item RequiredRole
This is a string specifying which users can access this page.

=back

Both parameters are passed to the PageBase constructor.

=back
=cut

sub new($$$@)
{
  my $class = shift;
  my ($Request, $_RequiredRole) = @_;

  my $self = {Request => $Request,
              CGIObj => CGI->new($Request),
              # ErrMessage => undef by default
              # ErrField => undef by default
  };
  $self = bless $self, $class;
  $self->{PageBase} = &$PageBaseCreator($self, @_);
  $self->_initialize(@_);
  return $self;
}

sub _initialize($$$)
{
  #my ($self, $Request, $RequiredRole) = @_;
}

=pod
=over 12

=item C<SetPageBaseCreator()>

Sets the function to be used to instantiate a new PageBase object for this
website. The resulting object can be accessed through GetPageBase().

=back
=cut

sub SetPageBaseCreator($)
{
  ($PageBaseCreator) = @_;
}

sub GetPageBase($)
{
  my ($self) = @_;

  return $self->{PageBase};
}

sub CGI($)
{
  my ($self) = @_;

  return $self->{CGIObj};
}

sub escapeHTML($$)
{
  my ($self, $String) = @_;

  return $self->{CGIObj}->escapeHTML($String);
}

=pod
=over 12

=head1 C<JsQuote()>

Quotes a string for JavaScript code.

=back
=cut

sub JsQuote($$)
{
  my ($self, $String) = @_;

  $String =~ s~\\~\\\\~g;
  $String =~ s~"~\\\"~g;
  $String =~ s~\n~\\n~g;
  return "\"$String\"";
}

=pod
=over 12

=head1 C<SetRefreshInterval()>

Sets the page refresh interval in seconds.

This can be called at any point before GenerateHttpHeaders().

=back
=cut

sub SetRefreshInterval($$)
{
  my ($self, $Seconds) = @_;

  $self->{PageBase}->SetRefreshInterval($Seconds);
}


#
# CGI parameters support
#

=pod
=over 12

=head1 C<GetParamNames()>

Returns the list of parameter names.

=back
=cut

sub GetParamNames($)
{
  my $self = shift;

  return $self->{CGIObj}->param();
}

=pod
=over 12

=head1 C<GetParam()>

Returns the specified parameter value.
Note that unlike CGI::param() this forces the result to scalar context to
avoid security issues.

To get the list of parameter names use GetParamNames().

=back
=cut

sub GetParam($$)
{
  my ($self, $Name) = @_;

  return scalar($self->{CGIObj}->param($Name));
}

=pod
=over 12

=head1 C<SetParam()>

Sets the specified parameter or removes it from the parameters list if the
value is undefined.

To get the list of parameter names use GetParamNames().

=back
=cut

sub SetParam($$$)
{
  my ($self, $Name, $Value) = @_;

  if (defined $Value)
  {
    $self->{CGIObj}->param($Name, $Value);
  }
  else
  {
    $self->{CGIObj}->delete($Name);
  }
}


#
# Session management
#

sub UnsetCookies($)
{
  my ($self) = @_;

  $self->{PageBase}->UnsetCookies();
}

sub SetCookies($)
{
  my ($self) = @_;

  $self->{PageBase}->SetCookies();
}

sub GetCurrentSession($)
{
  my ($self) = @_;

  return $self->{PageBase}->GetCurrentSession();
}

sub SetCurrentSession($$)
{
  my ($self, $Session) = @_;

  $self->{PageBase}->SetCurrentSession($Session);
}

sub Redirect($$)
{
  my ($self, $Location) = @_;

  return $self->{PageBase}->Redirect($Location);
}


#
# Error handling framework
#

=pod
=over 12

=head1 C<GetErrMessage()>

Returns the error message if any, undef otherwise.

=back
=cut

sub GetErrMessage($)
{
  my ($self) = @_;
  return $self->{ErrMessage};
}

=pod
=over 12

=head1 C<GetFirstErrField()>

Returns the name of the first invalid field if any, undef otherwise.

Note that if the error does not concern a specific field this may return
undef even though an error has been set.

=back
=cut

sub GetFirstErrField($)
{
  my ($self) = @_;
  return $self->{ErrFields} ? (sort keys %{$self->{ErrFields}})[0] : undef;
}

=pod
=over 12

=head1 C<GetErrFields()>

Returns a list containing the names of the invalid field(s).

Note that if the error does not concern a specific field this may return
an empty list even though an error has been set.

=back
=cut

sub GetErrFields($)
{
  my ($self) = @_;
  return $self->{ErrFields} ? keys %{$self->{ErrFields}} : ();
}

=pod
=over 12

=head1 C<IsErrField()>

Returns true if the specified field needs to be fixed.

=back
=cut

sub IsErrField($$)
{
  my ($self, $FieldName) = @_;
  return $self->{ErrFields}->{$FieldName};
}

=pod
=over 12

=head1 C<AddError()>

Adds an error message to be shown to the user.
=over

=item ErrMessage
A message to be shown on the page that describes why the form data is invalid.
This should include the display name of the field(s) that need to be fixed.
If this parameter is false, the error message and the list of invalid fields
are not modified.

=item ErrFields
A list of field names related to the error. This can be used by the page to
highlight the field(s) that needs to be corrected.

=back

Returns the ErrMessage parameter. This allows checking whether an error
was actually added.

=back
=cut

sub AddError($$@)
{
  my $self = shift;
  my $ErrMessage = shift;

  if ($ErrMessage)
  {
    $self->{ErrMessage} = "$self->{ErrMessage}\n$ErrMessage";
    foreach my $ErrField (@_)
    {
      $self->{ErrFields}->{$ErrField} = 1;
    }
  }
  return $ErrMessage;
}

sub ResetErrors($)
{
  my ($self) = @_;

  delete $self->{ErrMessage};
  delete $self->{ErrFields};
}

sub GenerateErrorDiv($)
{
  my ($self) = @_;

  $self->{PageBase}->GenerateErrorDiv($self);
}

sub GenerateErrorPopup($)
{
  my ($self) = @_;

  $self->{PageBase}->GenerateErrorPopup($self);
}


#
# HTML page generation
#

=pod
=over 12

=head1 C<GetPageTitle()>

This returns the page title as put in the HTML header.
Note that this may not be valid HTML and thus may need escaping.

=back
=cut

sub GetPageTitle($)
{
  my ($self) = @_;

  return $self->{PageBase}->GetPageTitle($self);
}

=pod
=over 12

=head1 C<GetTitle()>

This returns the title for the current web page or email section.
Note that this may not be valid HTML and thus may need escaping.

=back
=cut

sub GetTitle($)
{
  #my ($self) = @_;
  return undef;
}

sub GenerateImportJS($$)
{
  my ($self, $Filename) = @_;

  $self->{PageBase}->GenerateImportJS($Filename);
}

sub GenerateHttpHeaders($)
{
  my ($self) = @_;

  $self->{PageBase}->GenerateHttpHeaders($self);
}

sub GenerateHeader($)
{
  my ($self) = @_;

  $self->{PageBase}->GenerateHeader($self);
}

sub GenerateBody($)
{
  my ($self) = @_;

  die "Pure virtual function " . ref($self) . "::GenerateBody called";
}

sub GenerateFooter($)
{
  my ($self) = @_;

  $self->{PageBase}->GenerateFooter($self);
}

sub GeneratePage($)
{
  my ($self) = @_;

  $self->GenerateHttpHeaders();
  $self->GenerateHeader();
  $self->GenerateBody();
  $self->GenerateFooter();
}

1;
