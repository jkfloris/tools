USE winetestbot;

ALTER TABLE Patches
  MODIFY Id INT NOT NULL AUTO_INCREMENT,
  MODIFY WebPatchId INT NULL;

ALTER TABLE PendingPatches
  MODIFY PatchId INT NOT NULL;

ALTER TABLE Jobs
  MODIFY Id INT NOT NULL AUTO_INCREMENT,
  MODIFY PatchId INT NULL;

ALTER TABLE Steps
  MODIFY JobId INT NOT NULL;

ALTER TABLE Tasks
  MODIFY JobId INT NOT NULL;

ALTER TABLE RecordGroups
  MODIFY Id INT NOT NULL AUTO_INCREMENT;

ALTER TABLE Records
  MODIFY RecordGroupId INT NOT NULL;
