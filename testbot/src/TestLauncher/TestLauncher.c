/* -*- Mode: C; c-basic-offset: 3; indent-tabs-mode: nil -*- */
/*
 * Verifies that the dlls needed for the test are present.
 *
 * Copyright 2009 Ge van Geldorp
 * Copyright 2012-2021 Francois Gouget
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <windows.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))


/*
 * Error handling
 */

#ifdef __GNUC__
# define FORMAT(fmt, arg1)    __attribute__((format (printf, fmt, arg1) ))
#else
# define FORMAT(fmt, arg1)
#endif
void Error(const char* Format, ...) FORMAT(1,2);

static const char *Name0;
void Error(const char* Format, ...)
{
    va_list valist;
    fprintf(stderr, "%s:error: ", Name0);
    va_start(valist, Format);
    vfprintf(stderr, Format, valist);
    va_end(valist);
}


/*
 * Configuration / wineprefix sanity checks (from winetest.exe)
 */

static BOOL is_wow64(void)
{
   BOOL is_wow64;
   BOOL (WINAPI *pIsWow64Process)(HANDLE hProcess, PBOOL Wow64Process);
   HANDLE hkernel32 = GetModuleHandleA("kernel32.dll");
   pIsWow64Process = (void *)GetProcAddress(hkernel32, "IsWow64Process");
   if (!pIsWow64Process || !pIsWow64Process(GetCurrentProcess(), &is_wow64))
      return FALSE;
   return is_wow64;
}

static BOOL running_under_wine (void)
{
   HMODULE module = GetModuleHandleA("ntdll.dll");

   if (!module) return FALSE;
   return (GetProcAddress(module, "wine_server_call") != NULL);
}

static BOOL check_mount_mgr(void)
{
   HANDLE handle = CreateFileA( "\\\\.\\MountPointManager", GENERIC_READ,
                                FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0 );
   if (handle == INVALID_HANDLE_VALUE) return FALSE;
   CloseHandle( handle );
   return TRUE;
}

static BOOL check_wow64_registry(void)
{
   char buffer[MAX_PATH];
   DWORD type, size = MAX_PATH;
   HKEY hkey;
   BOOL ret;

   if (!is_wow64()) return TRUE;
   if (RegOpenKeyA( HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion", &hkey ))
      return FALSE;
   ret = !RegQueryValueExA( hkey, "ProgramFilesDir (x86)", NULL, &type, (BYTE *)buffer, &size );
   RegCloseKey( hkey );
   return ret;
}

static BOOL check_display_driver(void)
{
   HWND hwnd = CreateWindowA( "STATIC", "", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
                              0, 0, GetModuleHandleA(0), 0 );
   if (!hwnd) return FALSE;
   DestroyWindow( hwnd );
   return TRUE;
}

static BOOL running_on_visible_desktop (void)
{
    HWND desktop;
    HMODULE huser32 = GetModuleHandleA("user32.dll");
    HWINSTA (WINAPI *pGetProcessWindowStation)(void);
    BOOL (WINAPI *pGetUserObjectInformationA)(HANDLE,INT,LPVOID,DWORD,LPDWORD);

    pGetProcessWindowStation = (void *)GetProcAddress(huser32, "GetProcessWindowStation");
    pGetUserObjectInformationA = (void *)GetProcAddress(huser32, "GetUserObjectInformationA");

    desktop = GetDesktopWindow();
    if (!GetWindowLongPtrW(desktop, GWLP_WNDPROC)) /* Win9x */
        return IsWindowVisible(desktop);

    if (pGetProcessWindowStation && pGetUserObjectInformationA)
    {
        DWORD len;
        HWINSTA wstation;
        USEROBJECTFLAGS uoflags;

        wstation = pGetProcessWindowStation();
        assert(pGetUserObjectInformationA(wstation, UOI_FLAGS, &uoflags, sizeof(uoflags), &len));
        return (uoflags.dwFlags & WSF_VISIBLE) != 0;
    }
    return IsWindowVisible(desktop);
}


/*
 * Missing dll and entry point detection.
 */

const char *Subtest;
static unsigned Failures = 0;
static unsigned Skips = 0;

BOOL CALLBACK DumpCriticalErrorDescription(HWND hWnd, LPARAM lParam)
{
   char Buffer[512];
   int Count;

   /* No nesting is expected */
   if (GetParent(hWnd) != (HWND)lParam) return TRUE;

   Count = GetClassNameA(hWnd, Buffer, ARRAY_SIZE(Buffer));
   if (!Count || strcmp(Buffer, "Static")) return TRUE;

   Count = GetWindowTextA(hWnd, Buffer, ARRAY_SIZE(Buffer));
   if (!Count) return TRUE;
   printf("| %s\n", Buffer);

   return TRUE;
}

BOOL CALLBACK DetectCriticalErrorDialog(HWND TopWnd, LPARAM lParam)
{
   const char* TestFileName = (char*)lParam;
   int Count, TestFileLen;
   char Buffer[512];
   BOOL IsSkip = TRUE;
   const char* Reason;

   /* The window pid does not match the CreateProcess() one so it cannot be
    * used for filtering. But do filter on top-level dialogs.
    */
   if (GetParent(TopWnd) || GetWindow(TopWnd, GW_OWNER)) return TRUE;

   Count = GetClassNameA(TopWnd, Buffer, ARRAY_SIZE(Buffer));
   if (!Count || strcmp(Buffer, "#32770")) return TRUE;

   /* The dialog title always starts with the executable name */
   TestFileLen = strlen(TestFileName);
   Count = GetWindowTextA(TopWnd, Buffer, ARRAY_SIZE(Buffer));
   if (!Count || strncmp(Buffer, TestFileName, TestFileLen)) return TRUE;

   /* Followed by a reason */
   Reason = NULL;
   if (/* Windows >= 7 */
       strcmp(Buffer + TestFileLen, " - System Error") == 0 ||
       /* Windows <= Vista */
       strcmp(Buffer + TestFileLen, " - Unable To Locate Component") == 0)
      Reason = "missing dll";
   else if (strcmp(Buffer + TestFileLen, " - Entry Point Not Found") == 0)
      Reason = "missing entry point";
   else if (strcmp(Buffer + TestFileLen, " - Ordinal Not Found") == 0)
      Reason = "missing ordinal";
   else if (strncmp(Buffer + TestFileLen, " - ", 3) == 0)
      /* Old Windows versions used to translate the dialog so treat any
       * unrecognized critical error as a skip.
       */
      Reason = "unrecognized critical error";

   if (Reason)
   {
      IsSkip ? Skips++ : Failures++;
      /* An empty test unit name is allowed on the start, summary and done
       * lines, but not on individual failure / skip lines.
       */
      printf("%s.c:0: %s: %s (details below)\n",
             (*Subtest ? Subtest : "testlauncher"),
             (IsSkip ? "Tests skipped" : "Test failed"), Reason);
      printf("| %s\n", Buffer);
      EnumChildWindows(TopWnd, DumpCriticalErrorDescription, (LPARAM)TopWnd);
      /* Leave the dialog open so it appears on screenshots */
      return FALSE;
   }

   return TRUE;
}

DWORD Start;

DWORD RunTest(char *TestExeFileName, char* CommandLine, DWORD TimeOut, DWORD *Pid)
{
   STARTUPINFOA StartupInfo;
   PROCESS_INFORMATION ProcessInformation;
   DWORD ExitCode, WaitTimeOut;

   StartupInfo.cb = sizeof(STARTUPINFOA);
   GetStartupInfoA(&StartupInfo);
   StartupInfo.dwFlags |= STARTF_USESTDHANDLES;
   StartupInfo.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
   StartupInfo.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
   StartupInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);

   /* Unlike WineTest we do not have the luxury of first running the test with
    * a --list argument. This means we cannot use SetErrorMode() to check
    * whether there are missing dependencies as it could modify the test
    * results...
    */
   if (! CreateProcessA(NULL, CommandLine, NULL, NULL, TRUE, CREATE_DEFAULT_ERROR_MODE, NULL, NULL, &StartupInfo, &ProcessInformation))
   {
      *Pid = 0;
      if (GetLastError() == ERROR_SXS_CANT_GEN_ACTCTX)
      {
         Skips++;
         printf("%s.c:0: Tests skipped: Side-by-side dll version not found\n",
                (*Subtest ? Subtest : "testlauncher"));
         return 0;
      }
      return -GetLastError();
   }
   CloseHandle(ProcessInformation.hThread);
   *Pid = ProcessInformation.dwProcessId;

   WaitTimeOut = 100;
   ExitCode = WAIT_TIMEOUT;
   while (ExitCode == WAIT_TIMEOUT)
   {
      DWORD Elapsed, Remaining;

      ExitCode = WaitForSingleObject(ProcessInformation.hProcess, WaitTimeOut);
      Elapsed = GetTickCount() - Start;
      if (ExitCode != WAIT_TIMEOUT || Elapsed > TimeOut)
         break;

      /* ...instead detect the critical error dialog that pops up */
      EnumWindows(DetectCriticalErrorDialog, (LPARAM)TestExeFileName);
      if (Skips || Failures)
      {
         ExitCode = Failures ? Failures : WAIT_OBJECT_0;
         break;
      }

      Remaining = TimeOut == INFINITE ? TimeOut : TimeOut - Elapsed;
      WaitTimeOut = (Elapsed > 3000) ? Remaining :
                    (2 * WaitTimeOut < Remaining) ? 2 * WaitTimeOut :
                    Remaining;
   }

   if (ExitCode != WAIT_OBJECT_0)
   {
      switch (ExitCode)
      {
      case WAIT_FAILED:
         Error("Wait for child failed (error %lu)\n", GetLastError());
         break;

      case WAIT_TIMEOUT:
         /* The 'exit code' on the done line identifies timeouts */
         break;

      default:
         Error("Unexpected return value %lu from wait for child\n", ExitCode);
         break;
      }
      if (!TerminateProcess(ProcessInformation.hProcess, 257))
         Error("TerminateProcess failed (error %lu)\n", GetLastError());
   }
   else if (!Skips && !Failures &&
            !GetExitCodeProcess(ProcessInformation.hProcess, &ExitCode))
   {
      Error("Could not get the child exit code (error %lu)\n", GetLastError());
      ExitCode = 259;
   }
   CloseHandle(ProcessInformation.hProcess);
   return ExitCode;
}

/*
 * Command line parsing and test running.
 */

int main(int argc, char *argv[])
{
   int Arg;
   DWORD TimeOut;
   BOOL UsageError;
   char TestExeFullName[MAX_PATH];
   char *TestExeFileName;
   const char *Suffix;
   char TestName[MAX_PATH];
   int TestArg;
   char *CommandLine, *p;
   int CommandLen;
   DWORD Pid, ExitCode;

   Name0 = p = argv[0];
   while (*p != '\0')
   {
      if (*p == '/' || *p == '\\')
         Name0 = p + 1;
      p++;
   }

   /* Command line parsing and validation */

   TimeOut = INFINITE;
   CommandLine = NULL;
   Arg = 1;
   UsageError = FALSE;
   while (Arg < argc && ! UsageError)
   {
      if ((argv[Arg][0] == '-' || argv[Arg][0] == '/') && strlen(argv[Arg]) == 2)
      {
         if (argc <= Arg + 1)
            UsageError = TRUE;
         else if (argv[Arg][1] =='t')
         {
            char *EndPtr;
            TimeOut = (DWORD) strtoul(argv[Arg + 1], &EndPtr, 10) * 1000;
            if (*EndPtr != '\0')
            {
               Error("Invalid TimeOut value %s\n", argv[Arg + 1]);
               return 1;
            }
         }
         else
            UsageError = TRUE;
         Arg += 2;
      }
      else
      {
         if (GetFullPathNameA(argv[Arg], ARRAY_SIZE(TestExeFullName), TestExeFullName, &TestExeFileName) == 0)
         {
            Error("Could not get the test executable full path %s (error %lu)\n",
                    argv[Arg], GetLastError());
            return 1;
         }
         Suffix = strstr(TestExeFileName, "_test.exe");
         if (Suffix == NULL)
            Suffix = strstr(TestExeFileName, "_test64.exe");
         if (Suffix == NULL)
            Suffix = strchr(TestExeFileName, '.');
         if (Suffix == NULL)
            strcpy(TestName, TestExeFileName);
         else
         {
            strncpy(TestName, TestExeFileName, Suffix - TestExeFileName);
            TestName[Suffix - TestExeFileName] = '\0';
         }
         Subtest = (Arg + 1 < argc ? argv[Arg + 1] : "");

         CommandLen = strlen(TestExeFullName) + 3;
         for (TestArg = Arg + 1; TestArg < argc; TestArg++)
            CommandLen += 3 + strlen(argv[TestArg]);

         CommandLine = (char *) malloc(CommandLen);
         if (CommandLine == NULL)
         {
            Error("Unable to allocate memory for child command line\n");
            return 1;
         }

         CommandLine[0] = '"';
         strcpy(CommandLine + 1, TestExeFullName);
         strcat(CommandLine, "\"");
         for (TestArg = Arg + 1; TestArg < argc; TestArg++)
         {
            strcat(CommandLine, " \"");
            strcat(CommandLine, argv[TestArg]);
            strcat(CommandLine, "\"");
         }

         Arg = argc;
      }
   }
   if (CommandLine == NULL)
      UsageError = TRUE;
   if (UsageError)
   {
      fprintf(stderr, "Usage: %s [-t TimeOut] TestExecutable.exe [TestParameter...]\n", argv[0]);
      return 1;
   }

   /* Configuration / wineprefix sanity checks (from winetest.exe) */

   if (!running_on_visible_desktop())
      Error("Tests must be run on a visible desktop\n");

   if (running_under_wine())
   {
      if (!check_mount_mgr())
         Error("Mount manager not running, most likely your WINEPREFIX wasn't created correctly\n");

      if (!check_wow64_registry())
         Error("WoW64 keys missing, most likely your WINEPREFIX wasn't created correctly\n");

      if (!check_display_driver())
         Error("Unable to create a window, the display driver is not working\n");
   }

   /* Run the test */

   Start = GetTickCount();
   printf("%s:%s start -\n", TestName, Subtest);
   fflush(stdout);

   ExitCode = RunTest(TestExeFileName, CommandLine, TimeOut, &Pid);
   if (Failures || Skips)
      printf("%04lx:%s: %u tests executed (0 marked as todo, %u failures), %u skipped.\n", Pid, Subtest, Failures, Failures, Skips);

   printf("%s:%s:%04lx done (%ld) in %lds\n", TestName, Subtest,
          Pid, ExitCode, (GetTickCount() - Start) / 1000);

   return 0;
}
