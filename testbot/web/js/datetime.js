/* Format timestamps
 *
 * Copyright 2022 Francois Gouget
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
"use strict";

function Pad2(n)
{
    return n < 10 ? '0' + n : n;
}

function ShowDateTime(dom, attr)
{
    const sec1970 = dom.getAttribute("timestamp");
    const dt = new Date(sec1970 * 1000);
    const pretty = dt.getFullYear() +'-'+ Pad2(dt.getMonth() + 1) +'-'+
                   Pad2(dt.getDate()) +' '+ Pad2(dt.getHours()) +':'+
                   Pad2(dt.getMinutes()) +':'+ Pad2(dt.getSeconds());
    if (attr == null)
    {
        dom.innerHTML = pretty;
    }
    else
    {
        dom.setAttribute(attr, pretty);
    }
}

function ShowDayTimeTipDate(dom)
{
    const sec1970 = dom.getAttribute("timestamp");
    const dt = new Date(sec1970 * 1000);
    dom.setAttribute('title', dt.getFullYear() +'-'+ Pad2(dt.getMonth() + 1)
                              +'-'+ Pad2(dt.getDate()));
    dom.innerHTML = Pad2(dt.getDate()) +'&nbsp;'+ Pad2(dt.getHours()) +':'+
                    Pad2(dt.getMinutes()) +':'+ Pad2(dt.getSeconds());
}

function ShowTimeTipDate(dom)
{
    const sec1970 = dom.getAttribute("timestamp");
    const dt = new Date(sec1970 * 1000);
    dom.setAttribute('title', dt.getFullYear() +'-'+ Pad2(dt.getMonth() + 1)
                              +'-'+ Pad2(dt.getDate()));
    dom.innerHTML = Pad2(dt.getHours()) +':'+ Pad2(dt.getMinutes()) +':'+
                    Pad2(dt.getSeconds());
}

function ShowDateTipTime(dom)
{
    const sec1970 = dom.getAttribute("timestamp");
    const dt = new Date(sec1970 * 1000);
    dom.setAttribute('title', Pad2(dt.getHours()) +':'+ Pad2(dt.getMinutes())
                              +':'+ Pad2(dt.getSeconds()));
    dom.innerHTML = dt.getFullYear() +'-'+ Pad2(dt.getMonth() + 1) +'-'+
                    Pad2(dt.getDate());
}

function init()
{
    document.querySelectorAll(".datetime").forEach(dom => {
        ShowDateTime(dom);
    });
    document.querySelectorAll(".tipdatetime").forEach(dom => {
        ShowDateTime(dom, 'title');
    });
    document.querySelectorAll(".daytimetipdate").forEach(dom => {
        ShowDayTimeTipDate(dom);
    });
    document.querySelectorAll(".timetipdate").forEach(dom => {
        ShowTimeTipDate(dom);
    document.querySelectorAll(".datetiptime").forEach(dom => {
        ShowDateTipTime(dom);
    });
}

window.addEventListener('load', init);
