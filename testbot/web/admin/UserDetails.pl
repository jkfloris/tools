# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# User details page
#
# Copyright 2009 Ge van Geldorp
# Copyright 2013-2014, 2017-2018, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;


package RolesBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use URI::Escape;

use ObjectModel::BasicPropertyDescriptor;
use WineTestBot::Utils;


sub _initialize($$$)
{
  my ($self, $Collection, $EnclosingPage) = @_;

  $self->SUPER::_initialize($self, $Collection, $EnclosingPage);
  $self->{User} = $self->{EnclosingPage}->{Item};
  $self->{UserName} = $self->{User}->Name;
}

sub GetPropertyDescriptors($)
{
  my ($self) = @_;

  my %Props;
  map { $Props{$_->GetName()} = $_ } @{$self->{Collection}->GetPropertyDescriptors()};
  return [
    CreateBasicPropertyDescriptor($self->{UserName}, $self->{UserName}, !1, !1, "B", 1),
    $Props{IsDefaultRole},
    $Props{Name},
  ];
}

sub GetPropertyValue($$$)
{
  my ($self, $Row, $Col) = @_;

  return $Col->{Descriptor}->GetName() eq $self->{UserName} ?
         $self->{User}->Roles->HasItem($Row->{Item}->Name) :
         $self->SUPER::GetPropertyValue($Row, $Col);
}

sub GenerateFormStart($)
{
  my ($self) = @_;

  $self->SUPER::GenerateFormStart();
  print "<input type='hidden' name='Key' value='",
        uri_escape($self->{User}->GetKey()), "'>\n";
}

sub GetItemActions($)
{
  #my ($self) = @_;

  return ["Add role", "Remove role"];
}

sub OnItemAction($$$)
{
  my ($self, $Item, $Action) = @_;

  if ($self->{RW} and $Action eq "Add role")
  {
    return 1 if ($self->{User}->Roles->HasItem($Item->Name));

    $self->{User}->AddRole($Item);
    my ($_ErrKey, $ErrProperty, $ErrMessage) = $self->{User}->Roles->Save();
    if (defined $ErrMessage)
    {
      $self->{EnclosingPage}->AddError($ErrMessage, $ErrProperty);
      return 0;
    }
    NotifyAdministrator("New administrator user: $self->{UserName}",
                        "$self->{UserName} now is an administrator.\n");
    return 1;
  }
  if ($self->{RW} and $Action eq "Remove role")
  {
    my $Roles = $self->{User}->Roles;
    my $UserRole = $Roles->GetItem($Item->Name);
    return 1 if (!$UserRole);

    if ($self->{EnclosingPage}->AddError($Roles->DeleteItem($UserRole)))
    {
      return 0;
    }
    NotifyAdministrator("Removed an administrator: $self->{UserName}",
                        "$self->{UserName} is no longer an administrator.\n");
    return 1;
  }

  $self->{EnclosingPage}->AddError("No per-Item action defined for $Action");
  return 0;
}

sub GetActions($)
{
  #my ($self) = @_;
  return [];
}


package UserDetailsPage;

use ObjectModel::CGI::ItemPage;
our @ISA = qw(ObjectModel::CGI::ItemPage);

use WineTestBot::CGI::Sessions;
use WineTestBot::Config;
use WineTestBot::Roles;
use WineTestBot::Users;


sub _initialize($$$)
{
  my ($self, $Request, $RequiredRole) = @_;

  $self->SUPER::_initialize($Request, $RequiredRole, CreateUsers());
  if (!$self->{Item}->GetIsNew())
  {
    $self->{RolesBlock} = new RolesBlock(CreateRoles(), $self);
  }
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  if (defined($LDAPServer) &&
      ($PropertyName eq "Password" || $PropertyName eq "ResetCode"))
  {
    return "";
  }

  return $self->SUPER::DisplayProperty($PropertyDescriptor);
}

sub GenerateBody($)
{
  my ($self) = @_;

  $self->SUPER::GenerateBody();
  if ($self->{RolesBlock})
  {
    print "<h2><a name='Roles'></a>Roles</h2>\n";
    $self->{RolesBlock}->GenerateList();
  }
}

sub GetActions($)
{
  my ($self) = @_;

  my @Actions;
  if (!defined $LDAPServer and $self->{Item}->WaitingForApproval())
  {
    push @Actions, "Approve";
    push @Actions, "Reject" if ($self->{Item}->Name);
  }

  push @Actions, @{$self->SUPER::GetActions()};

  return \@Actions;
}

sub OnApprove($)
{
  my ($self) = @_;

  return !1 if (!$self->Save());
  return !1 if ($self->AddError($self->{Item}->Approve()));
  exit($self->RedirectToParent());
}

sub OnReject($)
{
  my ($self) = @_;

  $self->{Item}->Status('deleted');
  my ($ErrProperty, $ErrMessage) = $self->{Item}->Save();
  if (defined $ErrMessage)
  {
    $self->AddError($ErrMessage, $ErrProperty);
    return !1;
  }
  # Forcefully log out that user by deleting his web sessions
  DeleteSessions($self->{Item});
  exit($self->RedirectToParent());
}

sub OnSave($)
{
  my ($self) = @_;

  return !1 if (!$self->Save());
  if ($self->{Item}->Status ne 'active')
  {
    # Forcefully log out that user by deleting his web sessions
    DeleteSessions($self->{Item});
  }
  exit($self->RedirectToParent());
}

sub OnAction($$)
{
  my ($self, $Action) = @_;

  if ($Action eq "Approve")
  {
    return $self->OnApprove();
  }
  elsif ($Action eq "Reject")
  {
    return $self->OnReject();
  }
  elsif ($Action eq "Save")
  {
    return $self->OnSave();
  }
  elsif ($self->{RolesBlock} and
         grep { $_ eq $Action } @{$self->{RolesBlock}->GetItemActions()})
  {
    return $self->{RolesBlock}->OnAction($self->GetParam("Action"));
  }

  return $self->SUPER::OnAction($Action);
}


package main;

my $Request = shift;
my $Page = UserDetailsPage->new($Request, "admin");
$Page->GeneratePage();
