# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# User list page
#
# Copyright 2009 Ge van Geldorp
# Copyright 2013, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package UsersBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use URI::Escape;
use WineTestBot::CGI::Sessions;
use WineTestBot::Config;


sub Create($$)
{
  my ($Collection, $EnclosingPage) = @_;

  return UsersBlock->new($Collection, $EnclosingPage);
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $PropertyName =~ /^(?:Name|EMail|Status|RealName)$/ ? "ro" : "";
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  if ($Col->{Descriptor}->GetName() eq "Status")
  {
    my $User = $Row->{Item};
    my $Status = $User->Status;
    my ($Class, $Label);
    if ($Status eq "disabled")
    {
      ($Class, $Label) = ('userdisabled', 'disabled');
    }
    elsif ($Status eq "deleted")
    {
      ($Class, $Label) = ('userdeleted', 'deleted');
    }
    elsif ($User->WaitingForApproval())
    {
      ($Class, $Label) = ('userrequest', 'request');
    }
    elsif (!$User->Activated())
    {
      ($Class, $Label) = ('userapproved', 'approved');
    }
    elsif ($User->HasRole("admin"))
    {
      ($Class, $Label) = ('useradmin', 'admin');
    }
    elsif ($User->HasRole("wine-devel"))
    {
      ($Class, $Label) = ('userdevel', 'wine-devel');
    }
    else
    {
      ($Class, $Label) = ('usernone', 'none');
    }
    my $DetailsLink = $self->escapeHTML($self->GetDetailsLink($Row));
    print "<a href='$DetailsLink'><span class='$Class'>$Label</span></a>";
  }
  else
  {
    $self->SUPER::GenerateDataView($Row, $Col);
  }
}

sub GetItemActions($)
{
  my ($self) = @_;

  # LDAP accounts cannot be deleted
  return defined $LDAPServer ? [] : ["Delete"];
}

sub OnItemAction($$$)
{
  my ($self, $User, $Action) = @_;

  if ($Action eq "Delete")
  {
    $User->Status('deleted');
    my ($_ErrProperty, $ErrMessage) = $User->Save();
    if (defined $ErrMessage)
    {
      # Setting the error field is only useful on form pages
      $self->{EnclosingPage}->AddError($ErrMessage);
      return 0;
    }

    # Forcefully log out that user by deleting the web sessions
    DeleteSessions($User);
    return 1;
  }

  return $self->SUPER::OnItemAction($User, $Action);
}


package main;

use ObjectModel::CGI::CollectionPage;
use WineTestBot::Users;

my $Request = shift;
my $Page = ObjectModel::CGI::CollectionPage->new($Request, "admin", CreateUsers(), \&UsersBlock::Create);
$Page->GeneratePage();
