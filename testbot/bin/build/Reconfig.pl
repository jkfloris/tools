#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Performs the 'reconfig' task in the build machine. Specifically this updates
# the build machine's Wine repository, re-runs configure, and rebuilds the
# 32 and 64 bit winetest binaries.
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012-2014, 2017-2018 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
  $::BuildEnv = 1;
}

use File::Path;

use Build::Utils;
use WineTestBot::Config;
use WineTestBot::Missions;


#
# Build helpers
#

sub UpdateWineBuilds($$)
{
  my ($TaskMissions, $Flags) = @_;

  my $Configure = "--without-x --without-freetype --disable-winetest";
  return BuildWine($TaskMissions, $Flags, "exe32",
                  $Configure, ["buildtests"]) &&
         BuildWine($TaskMissions, $Flags, "exe64",
                   "$Configure --enable-win64", ["buildtests"]);
}


#
# Setup and command line processing
#

my ($Usage, $OptUpdate, $OptBuild, $MissionStatement);
my $OptBuildFlags = $Build::Utils::FROM_SCRATCH;
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--update")
  {
    $OptUpdate = 1;
  }
  elsif ($Arg eq "--build")
  {
    $OptBuild = 1;
  }
  elsif ($Arg eq "--no-rm")
  {
    $OptBuildFlags &= ~$Build::Utils::FROM_SCRATCH;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^-/)
  {
    Error "unknown option '$Arg'\n";
    $Usage = 2;
    last;
  }
  elsif (!defined $MissionStatement)
  {
    $MissionStatement = $Arg;
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}

# Check and untaint parameters
my $TaskMissions;
if (!defined $Usage)
{
  if (!$OptUpdate and !$OptBuild)
  {
    $OptUpdate = $OptBuild = 1;
  }
  $MissionStatement ||= "exe32:exe64";
  my ($ErrMessage, $Missions) = ParseMissionStatement($MissionStatement);
  if (defined $ErrMessage)
  {
    Error "$ErrMessage\n";
    $Usage = 2;
  }
  elsif (!@$Missions)
  {
    Error "Empty mission statement\n";
    $Usage = 2;
  }
  elsif (@$Missions > 1)
  {
    Error "Cannot specify missions for multiple tasks\n";
    $Usage = 2;
  }
  else
  {
    $TaskMissions = $Missions->[0];
  }
}
if (defined $Usage)
{
  my $Name0 = GetToolName();
  if ($Usage)
  {
    Error "try '$Name0 --help' for more information\n";
    exit $Usage;
  }
  print "Usage: $Name0 [--update] [--build [--no-rm]] [--help] [MISSIONS]\n";
  print "\n";
  print "Updates Wine to the latest version and recompiles it so the host is ready to build executables for the Windows tests.\n";
  print "\n";
  print "Where:\n";
  print "  --update     Update Wine's source code.\n";
  print "  --build      Update the Wine builds.\n";
  print "If none of the above actions is specified they are all performed.\n";
  print "  MISSIONS     Is a colon-separated list of missions. By default the following\n";
  print "               missions are run.\n";
  print "               - exe32: Build the 32 bit Windows test executables.\n";
  print "               - exe64: Build the 64 bit Windows test executables.\n";
  print "  --no-rm      Don't rebuild from scratch.\n";
  print "  --help       Shows this usage message.\n";
  exit 0;
}

if ($DataDir =~ /'/)
{
    LogMsg "The install path contains invalid characters\n";
    exit(1);
}
if (! -d "$DataDir/staging" and ! mkdir "$DataDir/staging")
{
    LogMsg "Unable to create '$DataDir/staging': $!\n";
    exit(1);
}


#
# Run the builds
#

exit(1) if ($OptBuild and !BuildNativeTestAgentd());
exit(1) if ($OptBuild and !BuildWindowsTestAgentd());
exit(1) if ($OptBuild and !BuildTestLauncher());
exit(1) if ($OptUpdate and !GitPull("wine"));
exit(1) if ($OptBuild and !UpdateWineBuilds($TaskMissions, $OptBuildFlags));

LogMsg "ok\n";
exit;
