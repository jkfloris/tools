# Shows or sets up the TestAgentd tasks
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

$Name0 = (Get-Item $MyInvocation.InvocationName).Basename


#
# Show the existing tasks
#

function ShowTask($Name)
{
  $Task = Get-ScheduledTask -TaskName $Name -ErrorAction SilentlyContinue
  if ($Task -ne $null)
  {
    Write-Output "$Name.User=$($Task.Principal.UserId)"
    Write-Output "$Name.RunLevel=$($Task.Principal.RunLevel)"
    Write-Output "$Name.Execute=$($Task.Actions.Execute)"
    Write-Output "$Name.Arguments=$($Task.Actions.Arguments)"
    Write-Output "$Name.WorkingDirectory=$($Task.Actions.WorkingDirectory)"
    Write-Output "$Name.NotOnBatteries=$($Task.Settings.DisallowStartIfOnBatteries)"
  }
  else
  {
    Write-Output "$Name=<no such task>"
  }
}

function ShowTasks()
{
  ShowTask "TestAgentd"
  ShowTask "TestAgentdAdm"
  exit 0
}


#
# Sets up split tasks
#

function SetupAdminTasks($Argv)
{
  $Port = [int]$Argv[1]
  $Task = Get-ScheduledTask -TaskName TestAgentd -ErrorAction SilentlyContinue
  if ($Task -eq $null)
  {
    echo "$Name0:error: the TestAgentd task does not exist!"
    exit 1
  }
  $WorkDir = $Task.Actions.WorkingDirectory
  if ($WorkDir -eq $null)
  {
    $WorkDir = Split-Path $Task.Actions.Execute
  }

  # Modify the 'elevated privileges' server to start on the alternate port
  $HighPort = $Port + 1
  $Action = New-ScheduledTaskAction -Execute $Task.Actions.Execute -Argument "--detach --set-time-only $HighPort" -WorkingDirectory $WorkDir
  Set-ScheduledTask -TaskName TestAgentd -Action $Action

  # Add a 'regular privileges' server on the regular port
  $Action = New-ScheduledTaskAction -Execute $Task.Actions.Execute -Argument "--detach --show-restarts $Port" -WorkingDirectory $WorkDir
  $AdmTask = Get-ScheduledTask -TaskName TestAgentdAdm -ErrorAction SilentlyContinue
  if ($AdmTask -eq $null)
  {
    Register-ScheduledTask -TaskName TestAgentdAdm -Action $Action -Trigger $Task.Triggers -Settings $Task.Settings
  }
  else
  {
    Set-ScheduledTask -TaskName TestAgentdAdm -Action $Action
  }
  exit 0
}


#
# Main
#

function ShowUsage()
{
  Write-Output "Usage: $Name0 show"
  Write-Output "or     $Name0 admin PORT"
  Write-Output ""
  Write-Output "Shows or modifies the Windows locales."
  Write-Output ""
  Write-Output "Where:"
  Write-Output "  show       Shows the existing TestAgentd tasks."
  Write-Output "  admin      Sets up a separate not-elevated-privileges task."
  Write-Output "  PORT       Specifies the port TestAgentd should listen on."
  Write-Output "  -?         Shows this help message."
}

$Action = $args[0]
if ($Action -eq "show") { ShowTasks }
if ($Action -eq "admin") { SetupAdminTasks $args }
$Rc = 0
if ($Action -and $Action -ne "-?" -and $Action -ne "-h" -and $Action -ne "help")
{
  echo "$Name0:error: unknown action $Action"
  $Rc = 2
}
ShowUsage
exit $Rc
