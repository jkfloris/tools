#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Updates or recreates the reference reports for the specified tasks and the
# latest directory.
#
# Copyright 2019-2020 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;


use File::Basename;

use WineTestBot::Config;
use WineTestBot::Jobs;
use WineTestBot::Log;
use WineTestBot::LogUtils;


#
# Logging and error handling helpers
#

my $Debug;
sub Debug(@)
{
  print STDERR @_ if ($Debug);
}

my $LogOnly;
sub Error(@)
{
  print STDERR "$Name0:error: ", @_ if (!$LogOnly);
  LogMsg @_;
}


#
# Setup and command line processing
#

my $Usage;
sub ValidateNumber($$)
{
  my ($Name, $Value) = @_;

  # Validate and untaint the value
  return $1 if ($Value =~ /^(\d+)$/);
  Error "$Value is not a valid $Name\n";
  $Usage = 2;
  return undef;
}

my ($OptDelete, $OptRebuild, $OptJobId, $OptStepNo, $OptTaskNo);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--delete")
  {
    $OptDelete = 1;
  }
  elsif ($Arg eq "--rebuild")
  {
    $OptRebuild = 1;
  }
  elsif ($Arg eq "--debug")
  {
    $Debug = 1;
  }
  elsif ($Arg eq "--log-only")
  {
    $LogOnly = 1;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^-/)
  {
    Error "unknown option '$Arg'\n";
    $Usage = 2;
    last;
  }
  elsif (!defined $OptJobId)
  {
    $OptJobId = ValidateNumber('job id', $Arg);
  }
  elsif (!defined $OptStepNo)
  {
    $OptStepNo = ValidateNumber('step number', $Arg);
  }
  elsif (!defined $OptTaskNo)
  {
    $OptTaskNo = ValidateNumber('task number', $Arg);
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}

# Check parameters
if (!defined $Usage)
{
  if ($OptDelete and $OptRebuild)
  {
    Error "--delete and --rebuild are mutually exclusive\n";
    $Usage = 2;
  }
}
if (defined $Usage)
{
  print "Usage: $Name0 [--rebuild] [--log-only] [--debug] [--help] [JOBID [STEPNO [TASKNO]]]\n";
  print "\n";
  print "Deletes, upgrades or recreates the reference reports and .errors files for the specified tasks and the latest directory.\n";
  print "\n";
  print "Where:\n";
  print "  --delete      Delete generated files of both the latest directory and the\n";
  print "                tasks.\n";
  print "  --rebuild     Delete and recreate the generated files of both the latest\n";
  print "                directory and the tasks from the reports of the WineTest tasks,\n";
  print "                based on their completion time. For tasks older than the first\n";
  print "                completed WineTest task, the individual tasks' reference\n";
  print "                reports are preserved. If these tasks don't have reference\n";
  print "                reports, an error is shown.\n";
  print "                Without this option only the missing reference reports and\n";
  print "                .errors files are added.\n";
  print "  JOBID STEPNO TASKNO Only the tasks matching the specified JOBID, STEPNO and\n";
  print "                TASKNO will be modified.\n";
  print "  --log-only    Only print errors to the log. By default they also go to stderr.\n";
  print "  --debug       Print additional debugging information.\n";
  print "  --help        Shows this usage message.\n";

  exit $Usage;
}

sub TaskKeyStr($)
{
  my ($Task) = @_;
  return join("/", @{$Task->GetMasterKey()});
}

sub Path2TaskKey($)
{
  my ($Path) = @_;

  return "latest" if (!defined $Path);
  return "latest" if ($Path =~ m~/latest(?:/|$)~);
  $Path =~ s~^.*/jobs/(\d+/\d+(?:/\d+)?)(/.*)?$~$1~;
  return $Path;
}

sub Delete($;$)
{
  my ($Path, $Label) = @_;

  return 0 if (!-f $Path);

  my $Name = basename($Path);
  $Label = defined $Label ? " $Label" : "";
  Debug(Path2TaskKey($Path) .": Deleting$Label $Name\n");
  return 0 if (unlink $Path);

  Error "Could not delete $Path: $!\n";
  return 1;
}

sub BuildErrorsCache($$$$;$)
{
  my ($Dir, $LogName, $IsWineTest, $TaskTimedOut, $Task) = @_;

  Debug(Path2TaskKey($Dir) .": Creating $LogName.errors\n");
  my $LogInfo = $LogName =~ /\.report$/ ?
      ParseWineTestReport("$Dir/$LogName", $IsWineTest, $TaskTimedOut) :
      ParseTaskLog("$Dir/$LogName");
  return CreateLogErrorsCache($LogInfo, $Task);
}

# The list of files in latest/. This includes files deleted for redundancy.
my %LatestFiles;

sub DoUpdateLatestReport($$$)
{
  my ($Task, $ReportName, $SrcReportPath) = @_;

  # Keep the name as is when saving an existing reference report
  my $SrcIsRef = ($SrcReportPath =~ /-job\d+-/);
  my $RefReportName = $SrcIsRef ? basename($SrcReportPath) :
                                  $Task->GetRefReportName($ReportName);
  return 0 if ($LatestFiles{$RefReportName});
  $LatestFiles{$RefReportName} = 1;

  my $Rc = 0;
  my $LatestReportPath = "$DataDir/latest/$RefReportName";
  if (!defined $OptJobId and !$OptDelete and !-f $LatestReportPath)
  {
    # Add the reference report to latest/
    Debug("latest: Adding $RefReportName from ". Path2TaskKey($SrcReportPath) ."\n");

    my $ErrMessages = UpdateLatestReport($RefReportName, $SrcReportPath);
    foreach my $ErrMessage (@$ErrMessages)
    {
      Error "$ErrMessage\n";
      $Rc = 1;
    }
  }

  return $Rc;
}

sub MoveRefReport($$;$)
{
  my ($RefDir, $RefName, $NewDir) = @_;

  my $RefPath = "$RefDir/$RefName";
  my $Rc = Delete("$RefPath.err");
  return $Rc if (!-f $RefPath);

  $NewDir ||= $RefDir;
  my $NewName = $RefName;
  if ($RefName =~ /^([a-zA-Z0-9_]+)_((?:exe|win|wow)(?:32|64)[a-zA-Z0-9_]*\.report)$/)
  {
    # We don't know which job generated this reference log.
    # So use a fixed, arbitrary JobId.
    $NewName = "$1-job000000-$2";
  }

  my $NewPath = "$NewDir/$NewName";
  return 0 if ($RefPath eq $NewPath);

  my $TaskKey = Path2TaskKey($NewDir);
  if (-f $NewPath and -M $NewPath <= -M $RefPath)
  {
    # A WineTest job has probably completed after the upgrade already
    return Delete($RefPath);
  }

  if (-f $NewPath)
  {
    Error "Could not move '$RefName' because '$NewName' already exists and is older!\n";
    return 1;
  }

  my $RelRefDir = ($RefDir eq $NewDir) ? "" : "../";
  Debug("$TaskKey: $RelRefDir$RefName -> $NewName\n");
  if (!rename($RefPath, $NewPath))
  {
    Error "Could not move '$RefName' to '$NewName' for $TaskKey: $!\n";
    return 1;
  }

  return 0;
}

sub ProcessTaskLogs($$$$)
{
  my ($Step, $Task, $IsWineTest, $CollectOnly) = @_;

  my $Rc = 0;
  my $ReportNames;
  my $TaskDir = $Task->GetDir();

  if (!$CollectOnly)
  {
    # Upgrade the naming scheme of the task's old reference reports,
    # delete those that are not meant to exist.
    (my $ErrMessage, $ReportNames, my $_TaskMissions) = $Task->GetReportNames();
    if ($ErrMessage)
    {
      Error "$ErrMessage\n";
      $Rc = 1;
    }
    foreach my $ReportName (@$ReportNames)
    {
      my $StepReportName = $Task->VM->Name ."_$ReportName";
      my $StepReportPath = $Step->GetFullFileName($StepReportName);
      if (-f "$TaskDir/$ReportName")
      {
        $Rc += MoveRefReport(dirname($StepReportPath), $StepReportName, $TaskDir);
        # The old WineTest reference reports for Windows tasks may end up being
        # duplicated: one copy in the build step and another in the step for
        # the task. MoveRefReport() moved the latter, now ensure Delete() can
        # remove the former.
        $StepReportPath = $Step->GetFullFileName($StepReportName);
      }
      $Rc += Delete($StepReportPath, "unneeded");
      $Rc += Delete("$StepReportPath.err", "unneeded");
    }

    my %LogMap = (
      "log"         => "task.log",
      "log.err"     => "testbot.log",
      "old_log"     => "old_task.log",
      "old_log.err" => "old_testbot.log");
    while (my ($OldName, $NewName) = each %LogMap)
    {
      next if (!-f "$TaskDir/$OldName");
      if (-f "$TaskDir/$NewName")
      {
        Error TaskKeyStr($Task) .": Could not rename $OldName because $NewName already exists\n";
        $Rc = 1;
      }
      elsif (!rename("$TaskDir/$OldName", "$TaskDir/$NewName"))
      {
        Error TaskKeyStr($Task) .": Could not rename $OldName to $NewName: $!\n";
        $Rc = 1;
      }
    }

    # testbot.log is the only log which we expect to be empty.
    # There is not much point keeping it if that's the case.
    my $TestBotLog = "$TaskDir/testbot.log";
    if (-z $TestBotLog and (!-f "$TestBotLog.errors" or -z _))
    {
      $Rc += Delete($TestBotLog);
      $Rc += Delete("$TestBotLog.errors");
    }
  }

  if (($OptDelete or $OptRebuild) and !$CollectOnly)
  {
    # Save / delete the task's reference reports... all of them,
    # even if they were not supposed to exist (e.g. failed builds).
    foreach my $ReportName (@$ReportNames)
    {
      # Also match the files related/derived from the report for the cleanup
      my $RefReportPaths = $Task->GetRefReportPaths($ReportName, "(?:\.err|\.errors)?");
      # Sort the filenames to make the log easier to analyse.
      foreach my $RefReportName (sort { $b cmp $a } keys %$RefReportPaths)
      {
        my $RefReportPath = $RefReportPaths->{$RefReportName};
        if (!$OptDelete and (-f $RefReportPath or -f "$RefReportPath.errors"))
        {
          if (!-f "$RefReportPath.errors")
          {
            # (Re)Build the .errors file before adding the reference report to
            # latest/.
            my $ErrMessage = BuildErrorsCache($TaskDir, $RefReportName, 1, 0);
            if (defined $ErrMessage)
            {
              Error "$ErrMessage\n";
              $Rc = 1;
            }
          }

          # Save this report to latest/ in case it's not already present there
          # (this would be the case for the oldest tasks with --rebuild)
          $Rc += DoUpdateLatestReport($Task, $ReportName, $RefReportPath);
        }
        $Rc += Delete($RefReportPath);
        $Rc += Delete("$RefReportPath.err");
        $Rc += Delete("$RefReportPath.errors");
      }

      $Rc += Delete("$TaskDir/$ReportName.err");
    }

    # And clean up the files derived from the task's logs
    foreach my $LogName (@{GetLogFileNames($TaskDir, "+old")})
    {
      $Rc += Delete("$TaskDir/$LogName.err");
      $Rc += Delete("$TaskDir/$LogName.errors");
    }
  }

  if (!$OptDelete and !$CollectOnly and $Task->Status eq "completed")
  {
    # Take a snapshot of the reference reports older than this Task
    foreach my $ReportName (@{GetLogFileNames($TaskDir)})
    {
      next if ($ReportName !~ /\.report$/);

      Debug(TaskKeyStr($Task) .": Snapshotting the reference reports for $ReportName\n");
      my $ErrMessages = SnapshotLatestReport($Task, $ReportName);
      foreach my $ErrMessage (@$ErrMessages)
      {
        Error "$ErrMessage\n";
        $Rc = 1;
      }
    }
  }

  if (!$OptDelete and !$CollectOnly and $Task->Status !~ /^(?:queued|running)$/)
  {
    # And (re)build the .errors files, even if Status != 'completed' for
    # task.log and testbot.log.
    my $TaskTimedOut;
    if ($Task->Started and $Task->Ended)
    {
      my $Duration = $Task->Ended - $Task->Started;
      $TaskTimedOut = $Duration > $Task->Timeout;
    }
    foreach my $LogName (@{GetLogFileNames($TaskDir, "+old")})
    {
      $Rc += Delete("$TaskDir/$LogName.err");
      if ($LogName =~ /\.report$/)
      {
        my $RefReportPaths = $Task->GetRefReportPaths($LogName);
        while (my ($RefReportName, $RefReportPath) = each %$RefReportPaths)
        {
          $Rc += Delete("$RefReportPath.err");
          next if (-f "$RefReportPath.errors");
          my $ErrMessage = BuildErrorsCache($TaskDir, $RefReportName, 1, 0);
          if (defined $ErrMessage)
          {
            Error "$ErrMessage\n";
            $Rc = 1;
          }
        }
      }

      # Then for the report / log itself
      if (!-f "$TaskDir/$LogName.errors")
      {
        my $ErrMessage = BuildErrorsCache($TaskDir, $LogName, $IsWineTest, $TaskTimedOut, $Task);
        if (defined $ErrMessage)
        {
          Error "$ErrMessage\n";
          $Rc = 1;
        }
      }
    }
  }

  if (!$OptDelete and $Task->Status eq "completed" and $Step->Type eq "suite")
  {
    # Update the latest reference reports
    # WineTest runs that timed out are missing results which would cause false
    # positives. So don't use them as reference results. Also note that
    # WineTest tasks have a single WineTest run so a task timeout is the same
    # as a WineTest timeout.
    my $Duration = $Task->Ended - $Task->Started;
    if ($Duration < $Task->Timeout)
    {
      foreach my $ReportName (@{GetLogFileNames($TaskDir)})
      {
        next if ($ReportName !~ /\.report$/);
        next if (-z "$TaskDir/$ReportName");
        $Rc += DoUpdateLatestReport($Task, $ReportName, "$TaskDir/$ReportName");
      }
    }
  }

  return $Rc;
}

sub ProcessLatestReports()
{
  my $Rc = 0;
  my $LatestGlob = "$DataDir/latest/*.report";

  # Delete or rename the old reference reports
  foreach my $LatestReportPath (glob("$LatestGlob $LatestGlob.err"))
  {
    my $RefReportName = basename($LatestReportPath);
    next if ($RefReportName !~ /^([a-zA-Z0-9_]+_[a-zA-Z0-9_]+\.report)(?:\.err)?$/);
    $RefReportName = $1; # untaint
    $LatestReportPath = "$DataDir/latest/$RefReportName";

    if ($OptDelete or $OptRebuild)
    {
      $Rc += Delete($LatestReportPath);
      $Rc += Delete("$LatestReportPath.err");
    }
    else
    {
      # MoveRefReport() also deletes .err files
      $Rc += MoveRefReport("$DataDir/latest", $RefReportName);
    }
  }

  # Then perform cleanups and rebuild missing files
  foreach my $LatestReportPath (glob("$LatestGlob $LatestGlob.errors"))
  {
    my $RefReportName = basename($LatestReportPath);
    # Keep the regexp in sync with WineTestBot::Task::GetRefReportName()
    next if ($RefReportName !~ /^([a-zA-Z0-9_]+-job[0-9]+-[a-zA-Z0-9_]+\.report)(?:\.errors)?$/);
    $RefReportName = $1; # untaint
    $LatestReportPath = "$DataDir/latest/$RefReportName";
    $Rc += Delete("$LatestReportPath.err");

    if (!$OptJobId and ($OptDelete or $OptRebuild))
    {
      # These can be rebuilt from the task reports
      $Rc += Delete($LatestReportPath);
      $Rc += Delete("$LatestReportPath.errors");
    }
    elsif (!-f $LatestReportPath)
    {
      $Rc += Delete("$LatestReportPath.errors", "orphaned");
    }
    else
    {
      $LatestFiles{$RefReportName} = 1;
      if (!-f "$LatestReportPath.errors")
      {
        # Build the missing .errors file
        my $ErrMessage = BuildErrorsCache("$DataDir/latest", $RefReportName, 1, 0);
        if (defined $ErrMessage)
        {
          Error "$ErrMessage\n";
          $Rc = 1;
        }
      }
    }
  }

  return $Rc;
}

my $Rc = 0;
$Rc = ProcessLatestReports();

my @AllTasks;
foreach my $Job (@{CreateJobs()->GetItems()})
{
  foreach my $Step (@{$Job->Steps->GetItems()})
  {
    foreach my $Task (@{$Step->Tasks->GetItems()})
    {
      push @AllTasks, $Task if ($Task->Status !~ /^(?:queued|running)$/);
    }
  }
}

# Process the tasks in completion order so the reference logs
# are updated in the right order.
my $Jobs = CreateJobs();
foreach my $Task (sort { ($a->Ended || 0) <=> ($b->Ended || 0) } @AllTasks)
{
  my ($JobId, $StepNo, $TaskNo) = @{$Task->GetMasterKey()};
  my $Steps = $Jobs->GetItem($JobId)->Steps;
  my $Step = $Steps->GetItem($StepNo);
  my $IsWineTest = $Step->FileType eq "patch" ||
                   $Steps->GetItem(1)->FileType eq "patch";
  my $CollectOnly = ((defined $OptJobId and $OptJobId ne $JobId) or
                     (defined $OptStepNo and $OptStepNo ne $StepNo) or
                     (defined $OptTaskNo and $OptTaskNo ne $TaskNo));
  $Rc += ProcessTaskLogs($Step, $Task, $IsWineTest, $CollectOnly);
}

exit $Rc ? 1 : 0;
