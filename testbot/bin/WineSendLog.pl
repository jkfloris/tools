#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Sends the job log to the submitting user and informs the Wine Patches web
# site of the test results.
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012-2020 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;


use Algorithm::Diff;

use WineTestBot::Config;
use WineTestBot::Failures;
use WineTestBot::Jobs;
use WineTestBot::Log;
use WineTestBot::LogUtils;
use WineTestBot::StepsTasks;
use WineTestBot::Utils;


#
# Logging and error handling helpers
#

my $Debug;
sub Debug(@)
{
  print STDERR @_ if ($Debug);
}

sub DebugTee($@)
{
  my ($File) = shift;
  print $File @_;
  Debug(@_);
}

my $LogOnly;
sub Error(@)
{
  print STDERR "$Name0:error: ", @_ if (!$LogOnly);
  LogMsg @_;
}


#
# Log errors caching
#

sub ParseTaskLogs($$)
{
  my ($Step, $Task) = @_;

  my $TaskDir = $Task->GetDir();

  # testbot.log is the only log which we expect to be empty.
  # There is not much point keeping it if that's the case.
  my $TestBotLog = "$TaskDir/testbot.log";
  unlink $TestBotLog if (-z $TestBotLog and !-f "$TestBotLog.errors");

  my ($IsWineTest, $TaskTimedOut);
  if ($Task->Started and $Task->Ended)
  {
    my $Duration = $Task->Ended - $Task->Started;
    $TaskTimedOut = $Duration > $Task->Timeout;
    $IsWineTest = ($Step->Type eq "patch" or $Step->Type eq "suite");
  }
  foreach my $LogName (@{GetLogFileNames($TaskDir, "+old")})
  {
    next if (-f "$TaskDir/$LogName.errors");

    my $LogInfo = $LogName =~ /\.report$/ ?
        ParseWineTestReport("$TaskDir/$LogName", $IsWineTest, $TaskTimedOut) :
        ParseTaskLog("$TaskDir/$LogName");
    my $ErrMessage = CreateLogErrorsCache($LogInfo, $Task);
    Error "$ErrMessage\n" if (defined $ErrMessage);
  }
}

sub ParseJobLogs($)
{
  my ($Job) = @_;

  foreach my $Step (@{$Job->Steps->GetItems()})
  {
    foreach my $Task (@{$Step->Tasks->GetItems()})
    {
      ParseTaskLogs($Step, $Task);
    }
  }
}


#
# Reporting
#

sub GetTitle($$)
{
  my ($StepTask, $LogName) = @_;

  my $Label = GetLogLabel($LogName);
  if ($LogName !~ /\.report$/ and
      ($StepTask->Type eq "build" or $StepTask->VM->Type eq "wine"))
  {
    $Label = "build log";
  }

  return $StepTask->VM->Name . " ($Label)";
}

sub DumpLogAndErr($$)
{
  my ($File, $LogPath) = @_;

  if (open(my $LogFile, "<", $LogPath))
  {
    foreach my $Line (<$LogFile>)
    {
      $Line =~ s/\s*$//;
      print $File "$Line\n";
    }
    close($LogFile);
  }

  # And append the extra errors
  my $LogInfo = LoadLogErrors($LogPath);
  foreach my $GroupName (@{$LogInfo->{ErrGroupNames}})
  {
    my $Group = $LogInfo->{ErrGroups}->{$GroupName};
    # Extra groups don't have a line number
    next if ($Group->{LineNo});

    print $File "\n$GroupName\n";
    print $File "$_\n" for (@{$Group->{Errors}});
  }
}

sub SendLog($)
{
  my ($Job) = @_;

  #
  # Collect and count the new and old failures, etc.
  #

  my ($JobInfo, @New, %BugDescriptions);
  my ($NewCount, $OldCount, $BotCount, $NotRun) = (0, 0, 0, 0);
  my $FileType = "test executable";

  my $Failures = CreateFailures($Job);
  my $StepsTasks = CreateStepsTasks(undef, $Job);
  my $SortedStepsTasks = $StepsTasks->GetSortedItems();
  foreach my $StepTask (@$SortedStepsTasks)
  {
    if ($StepTask->Status =~ /^(?:canceled|skipped)$/)
    {
      $NotRun++;
      next;
    }

    my $TaskDir = $StepTask->GetTaskDir();
    my $LogNames = GetLogFileNames($TaskDir);
    my $TaskInfo = $JobInfo->{$StepTask->Id} = {};
    $TaskInfo->{LogNames} = $LogNames;
    $TaskInfo->{NewCount} = 0;
    $TaskInfo->{OldCount} = 0;
    if ($StepTask->Status eq "boterror")
    {
      $TaskInfo->{BotCount} = 1;
      $BotCount++;
    }
    $FileType = "patch" if ($StepTask->FileType eq "patch");

    foreach my $LogName (@$LogNames)
    {
      my $LogInfo = LoadLogErrors("$TaskDir/$LogName");
      next if (!defined $LogInfo->{BadLog} and !$LogInfo->{ErrCount});
      $TaskInfo->{$LogName} = $LogInfo;

      my $HasLogHeader;
      foreach my $GroupName (@{$LogInfo->{ErrGroupNames}})
      {
        my $HasGroupHeader;
        my $Group = $LogInfo->{ErrGroups}->{$GroupName};
        foreach my $ErrIndex (0..$#{$Group->{Errors}})
        {
          my $LineNo = $Group->{LineNos}->[$ErrIndex];
          my $LineFailureIds = $Group->{Failures}->{$ErrIndex};
          if ($LineFailureIds)
          {
            foreach my $FailureId (@$LineFailureIds)
            {
              my $Failure = $Failures->GetItem($FailureId);
              next if (!$Failure);
              $BugDescriptions{$Failure->BugId} = $Failure->BugDescription;
              # Add bug information to $LogInfo
              push @{$LogInfo->{Bugs}->{$LineNo}}, $Failure->BugId;
            }
            $TaskInfo->{OldCount}++;
          }
          elsif ($Group->{IsNew}->[$ErrIndex])
          {
            if (!$HasLogHeader)
            {
              push @New, "\n=== ". GetTitle($StepTask, $LogName) ." ===\n";
              $HasLogHeader = 1;
            }
            if (!$HasGroupHeader)
            {
              push @New, ($GroupName ? "\n$GroupName:\n" : "\n");
              $HasGroupHeader = 1;
            }
            push @New, "$Group->{Errors}->[$ErrIndex]\n";
            $TaskInfo->{NewCount}++;
          }
          else
          {
            $TaskInfo->{OldCount}++;
          }
        }
      }
      $NewCount += $TaskInfo->{NewCount};
      $OldCount += $TaskInfo->{OldCount};
    }
  }


  #
  # Send a job summary to the developer
  #

  my $JobURL = MakeOfficialURL("/JobDetails.pl?Key=". $Job->GetKey());
  my $To = $WinePatchToOverride || $Job->GetEMailRecipient();
  if (defined $To)
  {
    Debug("-------------------- Developer email --------------------\n");
    my $Sendmail;
    if ($Debug)
    {
      open($Sendmail, ">>&=", 1);
    }
    else
    {
      open($Sendmail, "|-", "/usr/sbin/sendmail -oi -t -odq");
    }
    print $Sendmail "From: $RobotEMail\n";
    print $Sendmail "To: $To\n";
    my $Subject = "TestBot job " . $Job->Id . " results";
    my $Description = $Job->GetDescription();
    if ($Description)
    {
      $Subject .= ": " . $Description;
    }
    print $Sendmail "Subject: $Subject\n";
    if ($Job->Patch and $Job->Patch->MessageId)
    {
      print $Sendmail "In-Reply-To: ", $Job->Patch->MessageId, "\n";
      print $Sendmail "References: ", $Job->Patch->MessageId, "\n";
    }
    print $Sendmail "\nHi,\n\n";

    if ($NewCount)
    {
      print $Sendmail <<"EOF";
It looks like your $FileType introduces some new failures. Please
investigate and fix them if they are indeed new. If they are not new,
fixing them anyway would help a lot. Otherwise please ask for the known
failures list to be updated.

EOF
    }
    if ($BotCount)
    {
      print $Sendmail <<"EOF";
$BotCount TestBot errors prevented a full analysis of your $FileType.
If the test caused the operating system (e.g. Windows) to crash or
reboot you will probably have to modify it to avoid that.
Other issues should be reported to the TestBot administrators.

EOF
    }
    elsif (!$NewCount and !$NotRun)
    {
      print $Sendmail <<"EOF";
Congratulations!
It looks like your $FileType passed the tests with flying colors.

EOF
    }
    if ($OldCount)
    {
      print $Sendmail <<"EOF";
There are some preexisting failures (not caused by your $FileType).
If you know how to fix them that would be helpful.
EOF
      if (%BugDescriptions)
      {
        print $Sendmail <<"EOF";
In particular some failures are tracked in the bug(s) below:
EOF
        foreach my $BugId (sort { $a <=> $b } keys %BugDescriptions)
        {
          print $Sendmail "* $BugId - $BugDescriptions{$BugId}\n";
          print $Sendmail "  $WineBugUrl$BugId\n";
        }
        print $Sendmail "\n";
      }
    }

    print $Sendmail "                                Failures\n";
    print $Sendmail "VM                   Status    New /  Old  Command\n";
    foreach my $StepTask (@$SortedStepsTasks)
    {
      next if ($StepTask->Status =~ /^(?:canceled|skipped)$/);

      my $TestFailures = $StepTask->TestFailures;
      $TestFailures = "" if (!defined $TestFailures);
      my $Status = $StepTask->Status ne "completed" ? $StepTask->Status :
                   $TestFailures ? "failed" :
                   "success";
      my $Cmd = "";
      $Cmd = $StepTask->FileName ." " if ($StepTask->FileType =~ /^exe/);
      $Cmd .= $StepTask->CmdLineArg if (defined $StepTask->CmdLineArg);

      printf $Sendmail "%-20s %-8s %4s / %4s  %s\n", $StepTask->VMName,
                       $Status, $JobInfo->{$StepTask->Id}->{NewCount},
                       $JobInfo->{$StepTask->Id}->{OldCount}, $Cmd;
    }
    if ($NotRun)
    {
      print $Sendmail <<"EOF";

Note that $NotRun additional tasks could not be run due to previous
errors. A full run may uncover more issues.

EOF
    }

    print $Sendmail <<"EOF";

The full results can be found at:
$JobURL

Your paranoid android.

EOF

    # Print the failures
    foreach my $StepTask (@$SortedStepsTasks)
    {
      my $TaskInfo = $JobInfo->{$StepTask->Id};
      my $TaskDir = $StepTask->GetTaskDir();
      foreach my $LogName (@{$TaskInfo->{LogNames}})
      {
        my $LogInfo = $TaskInfo->{$LogName};
        next if (!defined $LogInfo->{BadLog} and !$LogInfo->{ErrCount});

        print $Sendmail "\n=== ", GetTitle($StepTask, $LogName), " ===\n";
        print $Sendmail "$LogInfo->{BadLog}\n" if (defined $LogInfo->{BadLog});

        foreach my $GroupName (@{$LogInfo->{ErrGroupNames}})
        {
          print $Sendmail ($GroupName ? "\n$GroupName:\n" : "\n");
          my $Group = $LogInfo->{ErrGroups}->{$GroupName};
          foreach my $ErrIndex (0..$#{$Group->{Errors}})
          {
            my $LineNo = $Group->{LineNos}->[$ErrIndex];
            my $Bugs = $LogInfo->{Bugs}->{$LineNo};
            my $Prefix = $Bugs ? join(" ", sort { $a <=> $b } @$Bugs) :
                         $Group->{IsNew}->[$ErrIndex] ? "new" :
                         "old";
            print $Sendmail "[$Prefix] $Group->{Errors}->[$ErrIndex]\n";
          }
        }
      }
    }

    close($Sendmail);
  }

  # This is all for jobs submitted from the website
  if (!defined $Job->Patch)
  {
    Debug("Not a mailing list patch -> all done.\n");
    return;
  }


  #
  # Send a summary of the new errors to the mailing list
  #

  Debug("\n-------------------- Mailing list email --------------------\n");

  if (!$NewCount)
  {
    Debug("Found no error to report to the mailing list\n");
  }
  else
  {
    my $Sendmail;
    if ($Debug)
    {
      open($Sendmail, ">>&=", 1);
    }
    else
    {
      open($Sendmail, "|-", "/usr/sbin/sendmail -oi -t -odq");
    }
    print $Sendmail "From: $RobotEMail\n";
    if (defined $To)
    {
      print $Sendmail "To: $To\n";
      print $Sendmail "Cc: $WinePatchCc\n";
    }
    else
    {
      print $Sendmail "To: $WinePatchCc\n";
    }
    print $Sendmail "Subject: Re: ", $Job->Patch->Subject, "\n";
    if ($Job->Patch->MessageId)
    {
      print $Sendmail "In-Reply-To: ", $Job->Patch->MessageId, "\n";
      print $Sendmail "References: ", $Job->Patch->MessageId, "\n";
    }
    print $Sendmail "\nHi,\n\n";

    if ($NewCount)
    {
      print $Sendmail <<"EOF";
It looks like your $FileType introduced the new failures shown below.
Please investigate and fix them before resubmitting your $FileType.
If they are not new, fixing them anyway would help a lot. Otherwise
please ask for the known failures list to be updated.

EOF
    }
    if ($BotCount)
    {
      print $Sendmail <<"EOF";
$BotCount TestBot errors prevented a full analysis of your $FileType.
If the test caused the operating system (e.g. Windows) to crash or
reboot you will probably have to modify it to avoid that.
Other issues should be reported to the TestBot administrators.

EOF
    }
    if ($OldCount)
    {
      print $Sendmail <<"EOF";
The tests also ran into some preexisting test failures. If you know how
to fix them that would be helpful. See the TestBot job for the details:

EOF
    }
    print $Sendmail <<"EOF";
The full results can be found at:
$JobURL

Your paranoid android.

EOF

    print $Sendmail $_ for (@New);
    close($Sendmail);
  }


  #
  # Create a .testbot file for the patches website
  #

  my $Patch = $Job->Patch;
  if (defined $Patch->WebPatchId and -d "$DataDir/webpatches")
  {
    my $BaseName = "$DataDir/webpatches/" . $Patch->WebPatchId;
    Debug("\n-------------------- WebPatches report --------------------\n");
    Debug("-- $BaseName.testbot --\n");
    if (open(my $Result, ">", "$BaseName.testbot"))
    {
      # Only take into account new errors to decide whether the job was
      # successful or not.
      DebugTee($Result, "Status: ". ($NewCount + $BotCount + $NotRun ? "Failed" : "OK") ."\n");
      DebugTee($Result, "Job-ID: ". $Job->Id ."\n");
      DebugTee($Result, "URL: $JobURL\n");

      foreach my $StepTask (@$SortedStepsTasks)
      {
        my $TaskDir = $StepTask->GetTaskDir();
        foreach my $LogName (@{$JobInfo->{$StepTask->Id}->{LogNames}})
        {
          print $Result "=== ", GetTitle($StepTask, $LogName), " ===\n";
          DumpLogAndErr($Result, "$TaskDir/$LogName");
        }
      }
      print $Result "--- END FULL_LOGS ---\n";
      close($Result);
    }
    else
    {
      Error "Job ". $Job->Id .": Unable to open '$BaseName.testbot' for writing: $!";
    }
  }
}


#
# Setup and command line processing
#

my $Usage;
sub ValidateNumber($$)
{
  my ($Name, $Value) = @_;

  # Validate and untaint the value
  return $1 if ($Value =~ /^(\d+)$/);
  Error "$Value is not a valid $Name\n";
  $Usage = 2;
  return undef;
}

my ($JobId);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--debug")
  {
    $Debug = 1;
  }
  elsif ($Arg eq "--log-only")
  {
    $LogOnly = 1;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^-/)
  {
    Error "unknown option '$Arg'\n";
    $Usage = 2;
    last;
  }
  elsif (!defined $JobId)
  {
    $JobId = ValidateNumber('job id', $Arg);
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}

# Check parameters
if (!defined $Usage)
{
  if (!defined $JobId)
  {
    Error "you must specify the job id\n";
    $Usage = 2;
  }
}
if (defined $Usage)
{
  if ($Usage)
  {
    Error "try '$Name0 --help' for more information\n";
    exit $Usage;
  }
  print "Usage: $Name0 [--debug] [--help] JOBID\n";
  print "\n";
  print "Analyze the job's logs and notifies the developer and the patches website.\n";
  print "\n";
  print "Where:\n";
  print "  JOBID      Id of the job to report on.\n";
  print "  --debug    More verbose messages for debugging.\n";
  print "  --log-only Only send error messages to the log instead of also printing them\n";
  print "             on stderr.\n";
  print "  --help     Shows this usage message.\n";
  exit 0;
}

my $Job = CreateJobs()->GetItem($JobId);
if (!defined $Job)
{
  Error "Job $JobId doesn't exist\n";
  exit 1;
}


#
# Analyze the log, notify the developer and the Patches website
#

ParseJobLogs($Job);
SendLog($Job);

LogMsg "Log for job $JobId sent\n";

exit 0;
