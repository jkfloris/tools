#!/usr/bin/env -S python3 -B

"""
Determine who should review a given MR and assign them as reviewers
"""

import sys
import copy
import re
from urllib.parse import urljoin
import fnmatch
import requests
import argparse

from util import fetch_all, Settings

def empty_record():
    m = { 'name': None,
          'globs': [],
          'maintainers': [],
          'people': [],
          'maintainer_ids': [],
          'people_ids': [],
    }
    return copy.deepcopy(m)

def exclude_author(in_ids, in_names, author):
    out_ids = []
    out_names = []
    for i in range(len(in_ids)):
        if in_names[i] != author:
            out_ids.append(in_ids[i])
            out_names.append(in_names[i])
    return out_ids, out_names

class Assign:
    settings = None
    users = {}
    user_map = {}
    maintainers_map = []
    verbose = False

    def __init__(self, settings):
        self.settings = settings
        self.verbose = self.settings.DEBUG

        url = urljoin(self.settings.GITLAB_URL, f"api/v4/projects/{self.settings.GITLAB_PROJECT_ID}/users")
        self.users = fetch_all(url, self.settings)
        for u in self.users:
            self.user_map[u['name']] = u['id']

        self.get_maintainers_map()

    def append_maint_user(self, line, id_array, name_array):
        match = re.match("(.+)<", line)
        if match:
            name = match.group(1).strip()
        else:
            print("Malformed person in MAINTAINERS: {}".format(line), file=sys.stderr)
            return

        if name in self.user_map:
            id_array.append(self.user_map[name])
            name_array.append(name)
        elif self.verbose:
            print("Cannot find GitLab account for [{}]".format(name), file=sys.stderr)

    def get_maintainers_map(self):
        if self.settings.MAINTAINERS is None:
            return

        url = urljoin(self.settings.GITLAB_URL, f"{self.settings.GITLAB_PROJECT_NAME}/-/raw/master/{self.settings.MAINTAINERS}")
        r = requests.get(url, headers={"PRIVATE-TOKEN": self.settings.GITLAB_TOKEN})
        r.raise_for_status()

        m = empty_record()
        for utf_line in r.iter_lines():
            line = utf_line.decode(r.encoding)
            if not line or len(line) < 3:
                continue
            if line.find("F:\t") == 0:
                # Skip THE REST patterns for now
                if line[3:4] == '*':
                    continue
                glob = line[3:]
                #  The Wine patterns are a bit unusual.  They are file globs,
                #    but with an implicit trailing * if the target is a directory.
                if glob[-1] == '/':
                    glob += '*'
                m['globs'].append(glob)
            elif line.find("M:\t") == 0:
                self.append_maint_user(line[3:], m['maintainer_ids'], m['maintainers'])
            elif line.find("P:\t") == 0:
                self.append_maint_user(line[3:], m['people_ids'], m['people'])
            elif line.find("W:\t") == 0:
                pass
            elif len(m['globs']) == 0:
                m['name'] = line
            else:
                self.maintainers_map.append(m)
                m = empty_record()
                m['name'] = line

    def post_reviewers(self, mr_iid, reviewers):
        url = urljoin(self.settings.GITLAB_URL, f"api/v4/projects/{self.settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/")
        r = requests.put(url, headers={"PRIVATE-TOKEN": self.settings.GITLAB_TOKEN}, json={'reviewer_ids': reviewers})
        r.raise_for_status()

    def get_assignees_from_files(self, files, author):
        maintainer_ids = []
        people_ids = []
        maintainer_names = []
        people_names = []
        for m in self.maintainers_map:
            for glob in m['globs']:
                for f in files:
                    if fnmatch.fnmatch(f, glob):
                        maintainer_ids += m['maintainer_ids']
                        maintainer_names += m['maintainers']
                        people_ids += m['people_ids']
                        people_names += m['people']
        if len(maintainer_ids) > 0:
            return exclude_author(maintainer_ids, maintainer_names, author['name'])
        return exclude_author(people_ids, people_names, author['name'])

    def get_assignees_from_commits(self, version):
        author_names = []
        for c in version['commits']:
            if c['author_name'] != c['committer_name']:
                author_names.append(c['committer_name'])
        ids = []
        names = []
        for name in set(author_names):
            if name in self.user_map:
                ids.append(self.user_map[name])
                names.append(name)
            else:
                print(f"MR includes a commit by {name}, but unable to find a GitLab ID in order to assign as reviewer.")
        return ids, names

    def get_assignees(self, files, author, version):
        ids, names = self.get_assignees_from_files(files, author)
        commit_ids, commit_names = self.get_assignees_from_commits(version)
        return list(set(ids + commit_ids)), list(set(names + commit_names))

    def assign_reviewers(self, mr_iid, author, version, update_db):
        paths = []
        if 'diffs' not in version:
            return
        for d in version['diffs']:
            if 'new_path' in d:
                paths.append(d['new_path'])
        ids, names = self.get_assignees(paths, author, version)
        if len(ids) > 0:
            unique_names = list(set(names))
            print(f"Asking {unique_names} to review MR {mr_iid}")
            if update_db:
                self.post_reviewers(mr_iid, list(set(ids)))
        else:
            print(f"No suitable reviewers found for MR {mr_iid}")

def main(argv):
    """ Debug code; pass in a config file and the names of files you want to test """
    parser = argparse.ArgumentParser(description="Debug code to test the assignment code")
    parser.add_argument('settings', nargs=1)
    parser.add_argument('--author', nargs=1, help='name of the person supposed to have submitted the MR')
    parser.add_argument('--committer', action='append', help='repeat to specify multiple committers in our fake MR.')
    parser.add_argument('files', nargs='*', help='name of Wine files supposedly in MR to pattern match')
    args = parser.parse_args()
    settings = Settings(args.settings)
    print(args)
    a = Assign(settings)
    if args.author:
        author = { 'name': args.author[0] }
    else:
        author = { 'name': None }
    version = { 'commits' : [] }
    if args.committer:
        for commit in args.committer:
            vcommit = { 'author_name' : author['name'], 'committer_name': commit }
            version['commits'].append(vcommit)
    print(version)
    ids, names = a.get_assignees(args.files, author, version)
    for i in range(len(ids)):
        print("{}: {}".format(ids[i], names[i]))

if __name__ == "__main__":
    main(sys.argv)
