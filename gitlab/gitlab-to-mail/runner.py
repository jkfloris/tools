#!/usr/bin/env python3

# runner:  run gitlabtomail.py periodically
#  Historically, we would do this on request, when we had a change
#  However, when Alexandre would do a bunch of merges, that process
#  could bog down.  We choose to instead invoke this with a configurable poll.

import subprocess
import sys
import os
import datetime
import time

from util import Settings

def usage():
    print(f"Usage:")
    print(f"  {sys.argv[0]} name-of-cfg-file")
    print(f"Will periodically run configured command at configured interval.")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Error: you must supply only the name of a .cfg file.")
        usage()
        sys.exit(-1)

    os.chdir(os.path.dirname(sys.argv[0]))
    settings = Settings(sys.argv[1])

    if not settings.RUNNER_INTERVAL or not settings.RUNNER_COMMAND:
        print(f"Error: you must supply RUNNER_INTERVAL and RUNNER_COMMAND in {sys.argv[1]}")
        sys.exit(-1)

    while True:
        nextrun = time.time() + settings.RUNNER_INTERVAL
        print("{}running {}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S - '), settings.RUNNER_COMMAND))
        subprocess.call([ settings.RUNNER_COMMAND, sys.argv[1]])
        try:
            # Delay a minimum of 30 seconds
            delay = nextrun - time.time()
            if delay < 30:
                delay = 30
            time.sleep(delay)
        except KeyboardInterrupt:
            break

    print("Server stopped.")
